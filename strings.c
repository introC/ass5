/*****************
 * Yair Shpitzer
 * 313285942
* 01
* ass5
 ****************/

#include <stdlib.h>
#include <string.h>
#include "element.h"
#include "stack.h"

const char LEFT_NORMAL = '(';
const char RIGHT_NORMAL = ')';
const char LEFT_SQUARE = '[';
const char RIGHT_SQUARE = ']';
const char LEFT_CURLY = '{';
const char RIGHT_CURLY = '}';
const char LEFT_TRIANGLE = '<';
const char RIGHT_TRIANGLE = '>';

int isLegalString(char str[]){
    Stack *stack = initStack();
    Element element;

    int length = strlen(str);
    for (int i = 0; i < length; i++){
        if (str[i] == LEFT_NORMAL || str[i] == LEFT_SQUARE || str[i] == LEFT_CURLY || str[i] == LEFT_TRIANGLE) {
            element.c = str[i];
            push(stack, element);
        } else if (str[i] == RIGHT_NORMAL){
            if (pop(stack).c != LEFT_NORMAL){
                destroyStack(stack);
                return 0;
            }
        } else if (str[i] == RIGHT_SQUARE){
            if (pop(stack).c != LEFT_SQUARE){
                destroyStack(stack);
                return 0;
            }
        } else if (str[i] == RIGHT_CURLY){
            if (pop(stack).c != LEFT_CURLY){
                destroyStack(stack);
                return 0;
            }
        } else if (str[i] == RIGHT_TRIANGLE){
            if (pop(stack).c != LEFT_TRIANGLE){
                destroyStack(stack);
                return 0;
            }
        }
    }

    if (isStackEmpty(stack)){
        destroyStack(stack);
        return 1;
    }
    destroyStack(stack);
    return 0;
}