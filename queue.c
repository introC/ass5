/*****************
 * Yair Shpitzer
 * 313285942
* 01
* ass5
 ****************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "element.h"
#include "stack.h"

typedef struct {
    Stack* s1;
    Stack* s2;
} Queue;

/***********************************************************************************
 * Function name: initQueue
 * Input: none
 * Output: Queue*
 * Function Operation: the function creates a pointer to a Queue (returns NULL if
 *                     allocation failed), and initializes queue's s1 and s2
 **********************************************************************************/
Queue* initQueue(){
    Queue *queue = (Queue*) malloc(sizeof(Queue));
    // when allocation failed
    if (queue == NULL){
        printf("Error! queue's allocation failed - File: queue.c, Function: initQueue\n");
        return NULL;
    }

    queue->s1 = initStack();
    queue->s2 = initStack();

    return queue;
}

/******************************************************************************
 * Function name: destroyQueue
 * Input: Queue *queue
 * Output: void
 * Function Operation: the function frees the memory allocated for the queue
 *****************************************************************************/
void destroyQueue(Queue* queue){
    destroyStack(queue->s1);
    destroyStack(queue->s2);
    free(queue);
}

/***************************************************************
 * Function name: isQueueEmpty
 * Input: Queue* queue
 * Output: int (0 or 1)
 * Function Operation: the function checks if queue is empty
 **************************************************************/
int isQueueEmpty(Queue* queue){
    if (isStackEmpty(queue->s1) && isStackEmpty(queue->s2)){
        return 1;
    }
    return 0;
}

/********************************************************************************
 * Function name: lenOfQueue
 * Input: Queue* queue
 * Output: int
 * Function Operation: the function returns the number of elements in the queue
 *******************************************************************************/
int lenOfQueue(Queue* queue){
    // number of elements in the queue is sum of the number of elements in queue's two stacks
    return lenOfStack(queue->s1) + lenOfStack(queue->s2);
}

/**********************************************************************
 * Function name: capacityOfQueue
 * Input: Queue* queue
 * Output: int
 * Function Operation: the function returns the capacity of the queue
 *********************************************************************/
int capacityOfQueue(Queue* queue){
    // the capacity of queue is the sum of it's two stacks' capacities
    return capacityOfStack(queue->s1) + capacityOfStack(queue->s2);
}

/********************************************************************************
 * Function name: enqueue
 * Input: Queue* queue, Element element
 * Output: void
 * Function Operation: the function enters the element to the head of the queue
 *******************************************************************************/
void enqueue(Queue* queue, Element element){
    push(queue->s1, element);
}

/********************************************************************************************
 * Function name: dequeue
 * Input: Queue* queue
 * Output: Element
 * Function Operation: the function removes the element at the head of queue and returns it
 *******************************************************************************************/
Element dequeue(Queue* queue){
    if (isStackEmpty(queue->s2)){
        int length = lenOfStack(queue->s1);
        for (int i = 0; i < length; i++){
            push(queue->s2, pop(queue->s1));
        }
    }
    return pop(queue->s2);
}

/******************************************************************
 * Function name: peek
 * Input: Queue* queue
 * Output: Element
 * Function Operation: the function returns the head og the queue
 *****************************************************************/
Element peek(Queue* queue){
    if (!isStackEmpty(queue->s2)) {
        return top(queue->s2);
    } else {
        /*
        the first element in s1 stack is the head of the queue.
        we can assume that s1 isn't empty because it's assumed the user won't send to this
        function unless queue isn't empty, and s2 is empty (according to condition)
        */
        return queue->s1->content[0];
    }
}