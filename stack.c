/*****************
* Yair Shpitzer
* 313285942
* 01
* ass5
 ****************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "element.h"

const int EMPTY_STACK_INDEX = -1;
const int INITIAL_SIZE = 1;

typedef struct {
    Element* content;
    int size;
    int topIndex;
} Stack;

/********************************************************************************************************************
 * Function name: initStack
 * Input: none
 * Output: Stack*
 * Function Operation: the function creates a pointer to a Stack, allocates memory for one element (returns NULL if
 *                     allocation failed), and initializes stack's size and topIndex
 *******************************************************************************************************************/
Stack* initStack(){
    Stack *stack = (Stack*) malloc(sizeof(Stack));
    // when allocation failed
    if (stack == NULL){
        printf("Error! stack's memory allocation - File: stack.c, Function: initStack\n");
        return NULL;
    }

    stack->content = (Element*) malloc(sizeof(Element));
    // when allocation failed
    if (stack->content == NULL){
        printf("Error! content's memory allocation - File: stack.c, Function: initStack\n");
        free(stack);
        return NULL;
    }

    stack->size = INITIAL_SIZE;
    stack->topIndex = EMPTY_STACK_INDEX;

    return stack;
}

/******************************************************************************
 * Function name: destroyStack
 * Input: Stack* stack
 * Output: void
 * Function Operation: the function frees the memory allocated for the stack
 *****************************************************************************/
void destroyStack(Stack* stack){
    free(stack->content);
    free(stack);
}

/***************************************************************
 * Function name: isStackEmpty
 * Input: Stack* stack
 * Output: int (0 or 1)
 * Function Operation: the function checks if stack is empty
 **************************************************************/
int isStackEmpty(Stack* stack){
    if (stack->topIndex == EMPTY_STACK_INDEX){
        return 1;
    }
    return 0;
}

/********************************************************************************
 * Function name: lenOfStack
 * Input: Stack* stack
 * Output: int
 * Function Operation: the function returns the number of elements in the stack
 *******************************************************************************/
int lenOfStack(Stack* stack){
    return stack->topIndex + 1;
}

/******************************************************************
 * Function name: capacityOfStack
 * Input: Stack* stack
 * Output: int
 * Function Operation: the function returns the size of the stack
 *****************************************************************/
int capacityOfStack(Stack* stack){
    return stack->size;
}

/********************************************************************************
 * Function name: push
 * Input: Stack* stack, Element element
 * Output: void
 * Function Operation: the function enters the element to the head of the stack
 *******************************************************************************/
void push(Stack* stack, Element element){
    // push is valid only if it will leave available room in stack, otherwise stack need to be increased
    if (capacityOfStack(stack) - lenOfStack(stack) > 1){
        stack->content[++stack->topIndex] = element;
    } else {
        // the temp pointer is used for saving the stack in case the realloc fails
        Element* temp;
        temp = (Element*) realloc(stack->content, 2 * stack->size * sizeof(Element));
        // when allocation failed
        if (temp == NULL){
            printf("Error! reallocation failed - File: stack.c, Function: push\n");
            stack->content[++stack->topIndex] = element;
        } else {
            stack->size *= 2;
            stack->content = temp;
            temp = NULL;
            stack->content[++stack->topIndex] = element;
        }
    }
}

/******************************************************************************************
 * Function name: pop
 * Input: Stack* stack
 * Output: Element
 * Function Operation: the function removes the top element from the stack and returns it
 *****************************************************************************************/
Element pop(Stack* stack){
    Element topElement = stack->content[stack->topIndex--];
    /*
    if removing element from stack will cause the length of the stack to equal
    less than half of the stack's size, the stack need to decreased
    */
    if (lenOfStack(stack) < (capacityOfStack(stack) / 2)){
        // the temp pointer is used for saving the stack in case the realloc failes
        Element* temp;
        temp = (Element*) realloc(stack->content, (stack->size/2) * sizeof(Element));
        // when allocation failed
        if (temp == NULL){
            printf("Error! reallocation failed - File: stack.c, Function: pop\n");
            return topElement;
        } else {
            stack->size /= 2;
            stack->content = temp;
            temp = NULL;
            return topElement;
        }
    } else {
        return topElement;
    }
}

/******************************************************************
 * Function name: top
 * Input: Stack* stack
 * Output: Element
 * Function Operation: the function returns the top of the stack
 *****************************************************************/
Element top(Stack* stack){
    return stack->content[stack->topIndex];
}

/************************************************************************
 * Function name: printStack
 * Input: Stack* stack
 * Output: void
 * Function Operation: the function prints the stack from top to bottom
 ***********************************************************************/
void printStack(Stack* stack){
    for (int i = stack->topIndex; i >=0; i--){
        printf("%d: ", i + 1);
        printElement(stack->content[i]);
        printf("\n");
    }
}